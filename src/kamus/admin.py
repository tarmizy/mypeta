from django.contrib import admin
from kamus.models import Kamus

# Register your models here.
class KamusAdmin(admin.ModelAdmin):
    """
    Creating input for Kamus
    """
    search_fields = ('title',)
    list_display = ('title', 'description')
    list_filter = ('title',)
    list_per_page = 15

admin.site.register(Kamus, KamusAdmin)