from django.shortcuts import render, redirect
from django.template import loader
from django.http import HttpResponse
from .models import Kamus
from .forms import kamusForm
from django.contrib import messages

# Create your views here.

def index(request):
    """
    View kamus
    """
    kamusList = Kamus.objects.order_by('created_at')
    template = loader.get_template('kamus.html')
    context = {
        "kamusList": kamusList,
    }
    # return HttpResponse(kamusList)
    return HttpResponse(template.render(context, request))

def createkamus(request):
    """
    Create Kamus
    """
    return render(request, 'create_kamus.html')

def savekamus(request):
    """
    Save kamus
    """
    if request.method == 'POST':
        form = kamusForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    messages.success(request, ("kamus Successfully Created"))
    return redirect('/kamus')

def editkamus(request, pk):
    """
    Edit kamus
    """
    kamus = Kamus.objects.get(id=pk)
    template = loader.get_template('edit_kamus.html')
    context = {
        "kamus": kamus,
    }
    return HttpResponse(template.render(context, request))

def updatekamus(request):
    """
    Update kamus
    """
    if request.method == 'POST':
        Kamus.objects.filter(id=request.POST.get('kamus_id')).update(
                    title=request.POST.get('title'),
                    description=request.POST.get('description')
                )
        messages.success(request, "kamus is Successfully Updated")
        return redirect("/kamus")

def deletekamus(request, pk):
    """
    Delete kamus
    """

    kamus = Kamus.objects.get(id=pk)
    kamus.delete()
    messages.success(request, "Kamus is Successfully Deleted")
    return redirect("/kamus")