"""
Forms Definition
"""
from django import forms
from kamus.models import Kamus

class kamusForm(forms.ModelForm):
    """
    Forms Definition for data process
    """
    class Meta(object):
        """
        Class meta for model process
        """
        model = Kamus
        fields = [
            'title',
            'description',
        ]
        