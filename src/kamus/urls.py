"""
Generate Urls for Helpdesk
"""
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    url(r'^$', views.index, name='kamus'),
    url(r'create', views.createkamus, name='kamuscreate'),
    url(r'savekamus', views.savekamus, name='savekamus'),
    url(r'edit/([0-9]+)/$', views.editkamus, name='editkamus'),
    url(r'updatekamus', views.updatekamus, name='updatekamus'),
    url(r'delete/([0-9]+)/$', views.deletekamus, name='deletekamus'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
