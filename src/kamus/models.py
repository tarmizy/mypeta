"""
Create Model for Kamus
"""
from django.db import models
from django.db.models import CharField
from django.utils.translation import gettext_lazy as _

# Create your models here.
class Kamus(models.Model):
    """
    Create model for Kamus
    """
    title = CharField(_("Title"), blank=True, max_length=255)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add='True')
    updated_at = models.DateTimeField(auto_now='True', blank='True', null='True')
    objects = models.Manager()

    class Meta():
        """
        class meta for naming model
        """
        verbose_name_plural = "Kamus"

    def __str__(self):
        return self.title