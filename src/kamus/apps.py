from django.apps import AppConfig


class KamusConfig(AppConfig):
    name = 'kamus'
